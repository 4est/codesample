//This is a code sample describing my style of coding and thought process.

//Naming conventions: It's important to have well thought out names that are descriptive to the context and what it is doing.
//Good naming conventions allow future developers understand the code and its purpose. This sample code is a mapping class

//Organization: I'm fan of keeping code organized and uniform. 
//I would rather take the extra few seconds to properly organize my code then to waste time later trying to remember what I wrote.
//And it goes back again to helping out the next developer who will have to read my code.

//I also try my best to follow DRY (Don't repeat yourself) and KISS (Keep it simple stupid). 
//It's better to spend the time to have the code be efficient and clean than fast and messy.

export class CodeSampleMappingPage {

    private _mappings: MappingModel;
    private _modal: MapAddingModal;
    private _codeSampleService: CodeSampleService;

    constructor() {
        this._codeSampleService = new CodeSampleService();
    }

    public init() {
        this._codeSampleService.getMappings().then(
            result => {

                this._mappings = new MappingModel(result);
                this._modal = new MapAddingModal(this._codeSampleService);

                new Vue({
                    el: '#cs-container',
                    data: () => {
                        return this._mappings;
                    },
                    methods: {
                        removeMap: (index: number) => { this.deleteMapping(index); },
                        toggleAddModal: () => this._modal.show(),
                        updateMap: (mapping: ICSMappingModel) => this._modal.show(mapping)
                    },
                });
            }
        )
    }

    public deleteMapping(id: number) {
        $.zeusConfirm(CodeSampleMappingResource.ConfirmDelete)
            .done(() => {
                this._codeSampleService.deleteMapping(id).then(
                    () => { window.location.reload(true); },
                    () => { console.log('Error occured while saving'); });
            });
    }
    public updateMapping(mapping: ISCWMappingModel) {
        this._codeSampleService.updateMapping(mapping).then(
            () => { window.location.reload(true); },
            () => { console.log('Error occured while saving'); });
    }

    private showError(errorMessage: string | string[]): void {
        var errorBoxSelector = 'templateWizardError';
        InfoBoxHelper.showErrorBox(errorBoxSelector, errorMessage, null, false);
    }
}

class MappingModel {
    public Mappings: IMappingModel[];
    private isAdding: boolean;
    public isUpdating: boolean;

    constructor(model?: MappingModel[]) {
        this.isAdding = false;

        if (model) {
            this.Mappings = model;
        }
        else {
            this.Mappings = [];
        }
    }
}




class CodeSampleService {
    public getMappings(): PromiseLike<Array<ICSMappingModel>> {
        return AjaxHelper.callGetJson('/Configuration/GetCSMappings');
    }

    public updateMapping(mapping: ICSMappingModel): PromiseLike<ICSMappingModel> {
        return AjaxHelper.callPostJson('/Configuration/UpdateCSMapping', mapping);
    }

    public addMapping(mapping: ICSMappingModel): PromiseLike<void> {
        return AjaxHelper.callPostJson('/Configuration/AddCSMapping', mapping);
    }

    public deleteMapping(id: number): PromiseLike<ICSMappingModel> {
        return AjaxHelper.callPostJson('/Configuration/DeleteCSMapping', { id: id });
    }

}

export interface IMappingModel {
    FodKingdom: string;
    FodCategory: string;
    FodSubcategory?: string;
    Category: string;
    SubCategory: string;
    Codes: ICodeModel;
    MappingId?: number
}

export interface ICodeModel {
    Type: string;
    CategoryCode: string;
    SubCode: string;
    LangCode?: string;
    FrameworkCode?: string;
}